@extends('customer.templates.default')

@section('title', 'Dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Conteúdo</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <h5 class="mb-2">Pastas</h5>

            <div class="row">
                @foreach($folders as $folder)
                <div class="col-sm-3">
                    <div class="info-box folder" onclick="window.location.href='{{route('customer.content.show', ['folder' => $folder])}}'">
                        <span class="info-box-icon bg-primary-ieaga"><i class="far fa-folder"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text" style="white-space: normal;">{{$folder->name}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                @endforeach
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection