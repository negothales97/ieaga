@extends('customer.templates.default')

@section('title', 'Dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-12">
                    <h1 class="m-0 text-dark">{{$folder->name}}</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            @if($subfolders->count()> 0 )
            <h5 class="mb-2">Pastas</h5>

            <div class="row">
                @foreach($subfolders as $subfolder)
                <div class="col-sm-3">
                    <div class="info-box folder"
                        onclick="window.location.href='{{route('customer.content.show', ['subfolder' => $subfolder])}}'">
                        <span class="info-box-icon bg-primary-ieaga"><i class="far fa-folder"></i></span>
                        <div class="info-box-content">
                            <span class="info-box-text" style="white-space:normal;">{{$subfolder->name}}</span>
                        </div>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                @endforeach
            </div>
            @endif
            
            @if($folder->videos()->count()> 0 )
            <h5 class="mb-2">Vídeos</h5>

            <div class="row">
                @forelse($folder->videos()->orderby('name', 'asc')->get() as $video)
                <div class="col-sm-3">
                    <div class="card card-widget widget-user">
                        <video class="video" data-name="{{$video->name}}" data-link="{{asset('uploads/'.$video->file)}}"
                            src="{{asset('uploads/'.$video->file)}}"></video>

                        <div class="card-video">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>{{$video->name}}</h5>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
                @empty
                <div class="row">
                    <div class="col-12">
                        <p class="ml-2">Nenhum vídeo cadastrado.</p>
                    </div>
                </div>
                @endforelse
            </div>
            
            @endif
            
            @if($folder->documents()->count()> 0 )
            <h5 class="mb-2">Documentos</h5>

            <div class="row">
                @forelse($folder->documents()->orderby('name', 'asc')->get() as $document)
                <div class="col-sm-3">
                    <div class="info-box">
                        <div class="info-box-content">
                            <span class="info-box-text" style="white-space: normal;">{{$document->name}}</span>
                        </div>
                        <a href="{{asset('uploads/'.$document->file)}}" data-download="{{$document->download}}"
                            data-name="{{$document->name}}" download data-pages="{{$document->pages}}"
                            class="btn-doc-download">
                            <span class="info-box-icon bg-success document"><i class="fas fa-download"></i></span>
                        </a>
                        <!-- /.info-box-content -->
                    </div>
                </div>
                @empty
                <div class="col">
                    <p>Nenhum arquivo nesta pasta</p>
                </div>
                @endforelse

            </div>
            @endif
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@section('scripts')

<script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.4.456/pdf.min.js" type="text/javascript"></script>

<script type="text/javascript">
$('.video').on('click', function() {
    let name = $(this).data('name');
    let link = $(this).data('link');
    $('#videoModal .modal-title').text(name);
    $('#videoModal .modal-body').html(
        `<video 
            class="video-js vjs-big-play-centered vjs-default-skin"
            controls
            preload="auto"
            width="640"
            height="400"
            style="width:100%"
            data-setup="{}"
        >
            <source src="${link}" type="video/mp4">
            <p class="vjs-no-js">
                To view this video please enable JavaScript, and consider upgrading to a
                web browser that
                <a href="https://videojs.com/html5-video-support/" target="_blank"
                >supports HTML5 video</a
                >
            </p>
        </video>
        `
    );
    // $("#videoModal .modal-body div").css("width", "100%");
    videojs(document.querySelector('.video-js'));
    $('#videoModal').modal('show');
});

$('#videoModal').on('hidden.bs.modal', function() {
    let player = videojs(document.querySelector('.video-js'));
    player.pause();
});

var pageNum = 0;
var numPages = 0;
var urlBroken = null;

$('.btn-doc-download').on('click', function(e) {
    numPages = $(this).data('pages');
    let url = $(this).attr('href');
    urlBroken = url.split(".");
    let extension = urlBroken[urlBroken.length - 1];
    if ($(this).data('download') != 1) {
        e.preventDefault();
        $('#img-box').css('display', 'none');
        $('#pdf-box').css('display', 'none');
        $('#pdf-loading').css('display', 'none');

        let name = $(this).data('name');
        if (extension === 'pdf') {

            pdfLoading();
            urlBroken[urlBroken.length - 1] = "-0.jpg";
            urlBroken[0] = `${urlBroken[0]}.`;
            urlBroken[1] = `${urlBroken[1]}.`;
            let file = urlBroken.join("");
            renderPage(file, pageNum);
            $('#page_count').html(numPages);
            $('#page_count_footer').html(numPages);
        } else {
            let bodyModal = `<img width="100%" src="${url}">`;
            $('#img-box').html(bodyModal);
            $('#img-box').css('display', 'block');
        }

        $('#documentModal .modal-title').text(name);
        $('#documentModal').modal('show');
    }
});

function pdfLoading() {
    $('#pdf-box').css('display', 'none');
    $('#pdf-loading').css('display', 'block');
}

function pdfReady() {
    $('#pdf-box').css('display', 'block');
    $('#pdf-loading').css('display', 'none');
}

function getNameFile(num) {
    urlBroken[urlBroken.length - 1] = `-${num}.jpg`;
    return urlBroken.join("");
}

function renderPage(file, num) {
    $('#documentCanvas').html(`<img width="100%" src="${file}">`);
    pdfReady();

    document.getElementById('page_num').textContent = num + 1;
    document.getElementById('page_num_footer').textContent = num + 1;
}

function onPrevPage() {
    if (pageNum <= 0) {
        return;
    }
    pageNum--;
    file = getNameFile(pageNum)
    renderPage(file, pageNum);
}

document.getElementById('prev').addEventListener('click', onPrevPage);
document.getElementById('prev-footer').addEventListener('click', onPrevPage);

/**
 * Displays next page.
 */
function onNextPage() {
    if (pageNum >= (numPages - 1)) {
        return;
    }
    pageNum++;
    file = getNameFile(pageNum)
    renderPage(file, pageNum);
}
document.getElementById('next').addEventListener('click', onNextPage);
document.getElementById('next-footer').addEventListener('click', onNextPage);
</script>
@endsection
@section('modals')
<div class="modal fade" id="videoModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body"></div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="documentModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div id="pdf-box">
                    <div>
                        <button id="prev" class="btn btn-default">Anterior</button>
                        <button id="next" class="btn btn-default">Próxima</button>
                        &nbsp; &nbsp;
                        <span>Página: <span id="page_num"></span> / <span id="page_count"></span></span>
                    </div>
                    <div id="documentCanvas"></div>
                    <div>
                        <button id="prev-footer" class="btn btn-default">Anterior</button>
                        <button id="next-footer" class="btn btn-default">Próxima</button>
                        &nbsp; &nbsp;
                        <span>Página: <span id="page_num_footer"></span> / <span id="page_count_footer"></span></span>
                    </div>
                </div>
                <div id="pdf-loading">
                    <img src="{{ asset('images/loading.gif')}}" alt="Carregando" />
                    <p>Carregando o documento...</p>
                </div>
                <div id="img-box"></div>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection