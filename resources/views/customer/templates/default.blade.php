<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">

<head>
    @include('customer.includes.head')
</head>

<body class="hold-transition sidebar-mini layout-fixed">

    @include('customer.includes.header')
    
    @include('customer.includes.sidebar')

    @yield('content')

    @include('customer.includes.modals')

    @include('customer.includes.footer')
    
    @include('customer.includes.mask')

</body>

</html>