<script src="https://unpkg.com/imask"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>

<script type="text/javascript">
function maskPhone() {
    let element = document.getElementsByClassName('input-phone');

    var maskOptions = {
        mask: '(00) 00000-0000'
    };
    for (item of element) {
        IMask(item, maskOptions);
    }
}
function maskCardNumber() {
    let element = document.getElementsByClassName('card_number');

    var maskOptions = {
        mask: '0000000000000000'
    };
    for (item of element) {
        IMask(item, maskOptions);
    }
}
function maskCep() {
    let element = document.getElementsByClassName('input-cep');

    var maskOptions = {
        mask: '00000-000'
    };
    for (item of element) {
        IMask(item, maskOptions);
    }
}
function maskExpirate() {
    let element = document.getElementsByClassName('expdate');

    var maskOptions = {
        mask: '00/0000'
    };
    for (item of element) {
        IMask(item, maskOptions);
    }
}
function maskCpf() {
    let element = document.getElementsByClassName('input-cpf');

    var maskOptions = {
        mask: '000.000.000-00'
    };
    for (item of element) {
        IMask(item, maskOptions);
    }
}
function maskCVV() {
    let element = document.getElementsByClassName('input-cvv');

    var maskOptions = {
        mask: '000'
    };
    for (item of element) {
        IMask(item, maskOptions);
    }
}

function maskMoney(element) {
    $(element).maskMoney({
        thousands: '.',
        decimal: ',',
        allowZero: true,
        symbolStay: true
    });
}

</script>