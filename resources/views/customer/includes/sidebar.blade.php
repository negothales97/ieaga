<aside class="main-sidebar sidebar-dark-ieaga elevation-4">
    <!-- Brand Logo -->
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class="d-block">{{Auth::guard('customer')->user()->name}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="{{route('customer.content.index')}}"
                        class="nav-link {{request()->is('customer/conteudo*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Conteúdo</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{route('customer.account.edit')}}"
                        class="nav-link {{request()->is('customer/account*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>Minha Conta</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('customer/logout')}}"
                        class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>