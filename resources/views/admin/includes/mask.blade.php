<script src="https://unpkg.com/imask"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>

<script type="text/javascript">
function maskPhone() {
    let element = document.getElementsByClassName('input-phone');

    var maskOptions = {
        mask: '(00) 00000-0000'
    };
    for (item of element) {
        IMask(item, maskOptions);
    }
}
function maskCpf() {
    let element = document.getElementsByClassName('input-cpf');

    var maskOptions = {
        mask: '000.000.000-00'
    };
    for (item of element) {
        IMask(item, maskOptions);
    }
}

function maskMoney(element) {
    $(element).maskMoney({
        thousands: '.',
        decimal: ',',
        allowZero: true,
        symbolStay: true
    });
}
function maskDate() {
    let element = document.getElementsByClassName('input-date');

    var maskOptions = {
        mask: '00/00/0000'
    };
    for (item of element) {
        IMask(item, maskOptions);
    }
}
maskDate();
</script>