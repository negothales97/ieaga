<aside class="main-sidebar sidebar-dark-ieaga elevation-4">
    <!-- Brand Logo -->
    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="info">
                <a href="#" class="d-block">{{Auth::guard('admin')->user()->name}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="{{route('admin.content.index')}}"
                        class="nav-link {{request()->is('admin/conteudo*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>Conteúdo</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{route('admin.customer.index')}}"
                        class="nav-link {{request()->is('admin/customer*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-users"></i>
                        <p>Usuários</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{route('admin.course.index')}}"
                        class="nav-link {{request()->is('admin/course*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-book"></i>
                        <p>Cursos</p>
                    </a>
                </li>
                @if(auth()->guard('admin')->user()->id < 5)
                <li class="nav-item has-treeview">
                    <a href="{{route('admin.admin.index')}}"
                        class="nav-link {{request()->is('admin/admin*') ? 'active' : ''}}">
                        <i class="fas fa-user-shield nav-icon "></i>
                        <p>Administradores</p>
                    </a>
                </li>
                @endif
                <li class="nav-item has-treeview">
                    <a href="{{route('admin.account.edit')}}"
                        class="nav-link {{request()->is('admin/account*') ? 'active' : ''}}">
                        <i class="nav-icon fas fa-cog"></i>
                        <p>Minha Conta</p>
                    </a>
                </li>
                <li class="nav-item has-treeview">
                    <a href="{{url('admin/logout')}}"
                        class="nav-link">
                        <i class="nav-icon fas fa-sign-out-alt"></i>
                        <p>Logout</p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>