@extends('admin.templates.default')

@section('title', 'Administradores')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Administradores</h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <div class="float-right">
                        <button class="btn btn-success act-include"
                            data-url="{{route('admin.admin.create')}}">Adicionar</button>
                    </div>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Lista de Administradores</h3>
                            <div class="card-tools">
                                <?php

                                $paginate = $resources;
                                $link_limit = 7;
                                ?>

                                @if($paginate->lastPage() > 1)
                                <nav>
                                    <ul class="pagination pagination-sm">
                                        <li class="page-item {{ ($paginate->currentPage() == 1) ? ' disabled' : '' }}">
                                            <a class="page-link {{ $paginate->url(1)}}" href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                        @for($i = 1; $i <= $paginate->lastPage(); $i++)
                                            <?php
                                            $half_total_links = floor($link_limit / 2);
                                            $from = $paginate->currentPage() - $half_total_links;
                                            $to = $paginate->currentPage() + $half_total_links;
                                            if ($paginate->currentPage() < $half_total_links) {
                                            $to += $half_total_links - $paginate->currentPage();
                                            }
                                            if ($paginate->lastPage() - $paginate->currentPage() < $half_total_links) {
                                            $from -= $half_total_links - ($paginate->lastPage() - $paginate->currentPage()) - 1;
                                            }
                                            ?>
                                            <li
                                                class="page-item {{ ($paginate->currentPage() == $i) ? ' active' : '' }}">
                                                <a class="page-link" href="{{ $paginate->url($i)}}">{{ $i }}</a>
                                            </li>
                                            @endfor
                                            <li
                                                class="page-item {{ ($paginate->currentPage() == $paginate->lastPage()) ? ' disabled' : '' }}">
                                                <a class="page-link" href=" {{ $paginate->url($paginate->lastPage())}}"
                                                    aria-label="Next">
                                                    <span aria-hidden="true">&raquo;</span>
                                                    <span class="sr-only">Next</span>
                                                </a>
                                            </li>
                                    </ul>
                                </nav>
                                @endif
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body p-3">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Nome</th>
                                        <th>E-mail</th>
                                        <th>Ação</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @forelse($resources as $resource)
                                    <tr>
                                        <td>{{$resource->name}}</td>
                                        <td>{{$resource->email}}</td>
                                        <td class="text-center align-middle py-0">
                                            <div class="btn-group btn-group-sm">
                                                <a href="{{route('admin.admin.edit', ['admin' => $resource])}}"
                                                    class="btn btn-primary-ieaga">
                                                    <i class="fa fa-edit"></i>
                                                </a>
                                                <button onclick="deleteItem(this, 1)"
                                                    data-href="{{route('admin.admin.delete', ['admin' => $resource])}}"
                                                    class="btn btn-danger act-delete">
                                                    <i class="fa fa-trash"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @empty
                                    <tr>
                                        <td colspan=3>Nenhum usuário cadastrado</td>
                                    </tr>
                                    @endforelse

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                </div>

                <!-- ./col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection