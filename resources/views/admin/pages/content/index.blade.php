@extends('admin.templates.default')

@section('title', 'Dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Conteúdo</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12 ">
                    <button class="btn btn-primary-ieaga btn-sm float-right btn-content" id="btnAddFolder"><i
                            class="far fa-folder"></i> Adicionar Pasta</button>

                </div>
            </div>
            <h5 class="mb-2">Pastas</h5>

            <div class="row">
                @forelse($folders as $folder)
                <div class="col-sm-3">
                    @component('admin.components.folder',['folder' => $folder])
                    @endcomponent

                </div>
                @empty
                <div class="col">
                    <p>Nenhuma pasta encontrada</p>
                </div>
                @endforelse
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$('#btnAddFolder').on('click', function() {
    $('#modalAddFolder').modal('show');
});

</script>
@endsection
@section('modals')
<div class="modal fade" id="modalAddFolder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Adicionar Pasta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.folder.store')}}" method="post">
                {{csrf_field()}}
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="input-name-folder">Nome</label>
                                <input type="text" class="form-control" name="name" id="input-name-folder" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection