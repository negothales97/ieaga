@extends('admin.templates.default')

@section('title', 'Dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-12">
                    <h1 class="m-0 text-dark">{{$folder->name}}</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">
                @php
                $url = $isMaster ? route('admin.content.index') : route('admin.content.show', ['folder' => $folder->folder_id]);
                @endphp
                    <a href="{{$url}}">
                        <button class="btn btn-primary-ieaga btn-sm btn-content" style="margin-left:0">
                            <i class="fas fa-arrow-left"></i> Voltar
                        </button>
                    </a>
                    <button class="btn btn-primary-ieaga btn-sm float-right btn-content" id="btnAddFolder"><i
                            class="far fa-folder"></i> Adicionar Pasta</button>
                    <button class="btn btn-primary-ieaga btn-sm float-right btn-content" id="btnAddVideo"><i
                            class="far fa-play-circle"></i>
                        Adicionar Vídeo</button>
                    <button class="btn btn-primary-ieaga btn-sm float-right btn-content" id="btnAddDocument"><i
                            class="far fa-file"></i> Adicionar
                        Documento</button>
                </div>
            </div>
            
            
            @if($folder->folders()->count()> 0 )
            <h5 class="mb-2">Pastas</h5>

            <div class="row">
                @forelse($folder->folders()->orderby('name', 'asc')->get() as $subfolder)
                <div class="col-sm-3">
                    @component('admin.components.folder',['folder' => $subfolder])
                    @endcomponent
                </div>
                @empty
                <div class="col">
                    <p>Nenhuma pasta encontrada</p>
                </div>
                @endforelse
            </div>
            
            @endif
            
            @if($folder->videos()->count()> 0 )
            <h5 class="mb-2">Vídeos</h5>

            <div class="row">
                @forelse($folder->videos()->orderby('name', 'asc')->get() as $video)
                <div class="col-sm-3">
                    <div class="card card-widget widget-user">
                        <div class="card-header">
                            <div class="card-tools">
                                <div class="btn-group btn-group-sm">
                                    <a href="#" class="btn btn-primary-ieaga btn-edit-video"
                                        data-url="{{route('admin.video.update', ['video' => $video])}}"
                                        data-name="{{$video->name}}">
                                        <i class="fas fa-pen"></i>
                                    </a>
                                    <a href="{{route('admin.video.delete', ['video' => $video])}}"
                                        class="btn btn-danger btn-delete">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <video class="video" data-name="{{$video->name}}" data-link="{{asset('uploads/'.$video->file)}}"
                            src="{{asset('uploads/'.$video->file)}}"></video>

                        <div class="card-video">
                            <div class="row">
                                <div class="col-sm-12">
                                    <h5>{{$video->name}}</h5>
                                    <div class="card-tools">

                                    </div>
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                    </div>
                </div>
                @empty
                <div class="col">
                    <p>Nenhuma vídeo encontrado</p>
                </div>
                @endforelse
            </div>
            @endif
            
            @if($folder->documents()->count()> 0 )
            <h5 class="mb-2">Documentos</h5>

            <div class="row">
                @forelse($folder->documents()->orderby('name', 'asc')->get() as $document)
                <div class="col-sm-3">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-tools">
                                <div class="btn-group btn-group-sm">
                                    @if(fileIsPdf($document->file) || fileIsImage($document->file))
                                        @if($document->download == 0)
                                        <a href="{{route('admin.document.change', ['document' => $document])}}"
                                            class="btn btn-primary-ieaga" title="Disponibilizar para download">
                                            <i class="fas fa-toggle-off"></i>
                                        </a>
                                        @else
                                        <a href="{{route('admin.document.change', ['document' => $document])}}"
                                            class="btn btn-primary-ieaga" title="Não disponibilizar para download">
                                            <i class="fas fa-toggle-on"></i>
                                        </a>
                                        @endif
                                    @endif

                                    <a href="#" class="btn btn-primary-ieaga btn-edit-document" data-name="{{$document->name}}"
                                        data-id="{{$document->id}}">
                                        <i class="fas fa-pen"></i>
                                    </a>
                                    <a href="{{route('admin.document.delete', ['document' => $document])}}"
                                        class="btn btn-danger btn-delete">
                                        <i class="fas fa-trash"></i>
                                    </a>
                                </div>
                            </div>
                            <!-- /.card-tools -->
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body">
                            <div class="info-box">
                                <div class="info-box-content">
                                    <span class="info-box-text" style="white-space: normal;">{{$document->name}}</span>
                                </div>
                                <a href="{{asset('uploads/'.$document->file)}}" download><span
                                        class="info-box-icon bg-success document"
                                        href="{{asset('uploads/'.$document->file)}}" download><i
                                            class="fas fa-download"></i></span></a>
                                <!-- /.info-box-content -->
                            </div>
                        </div>
                        <!-- /.card-body -->
                    </div>

                </div>
                @empty
                <div class="col">
                    <p>Nenhuma documento encontrado</p>
                </div>
                @endforelse
            </div>
            @endif

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$('.video').on('click', function() {
    let name = $(this).data('name');
    let link = $(this).data('link');
    $('#videoModal .modal-title').text(name);
    $('#videoModal .modal-body').html(
        `<video width="100%" controls controlsList="nodownload">
            <source src="${link}" type="video/mp4">
            Your browser does not support HTML video.
        </video>`
    );
    $('#videoModal').modal('show');
});
$('#btnAddVideo').on('click', function() {
    $('#modalAddVideo').modal('show');
});
$('#btnAddDocument').on('click', function() {
    $('#modalAddDocument').modal('show');
});
$('#btnAddFolder').on('click', function() {
    $('#modalAddFolder').modal('show');
});
$('.btn-edit-video').on('click', function() {
    let name = $(this).data('name');
    let url = $(this).data('url');
    $('#modalEditVideo input[name=name]').val(name);
    $('#modalEditVideo form').attr('action', url);
    $('#modalEditVideo').modal('show');
});
</script>
@endsection
@section('modals')
<div class="modal fade" id="videoModal">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

<div class="modal fade" id="modalAddDocument">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Adicionar Documento</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.document.store')}}" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="folder_id" value="{{$folder->id}}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="input-name-document">Nome documento</label>
                                <input type="text" class="form-control" name="name" id="input-name-document">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="input-file-document">Documento</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="file" class="custom-file-input"
                                            id="input-file-document">
                                        <label class="custom-file-label" for="input-file-document">Escolha um
                                            documento</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalEditDocument">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Editar Documento</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="#" method="post" enctype="multipart/form-data">
                {{csrf_field()}}
                <input type="hidden" name="folder_id" value="{{$folder->id}}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="input-name-document">Nome documento</label>
                                <input type="text" class="form-control" name="name" id="input-name-document">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Atualizar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalAddVideo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Adicionar Vídeo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.video.store')}}" method="post" enctype="multipart/form-data"
                autocomplete="off">
                {{csrf_field()}}
                <input type="hidden" name="folder_id" value="{{$folder->id}}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Nome do vídeo</label>
                                <input type="text" name="name" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="input-file">Vídeo</label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="file" class="custom-file-input" id="input-video-file">
                                        <label class="custom-file-label" for="input-file">Escolha um vídeo</label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalEditVideo">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Editar Vídeo</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="" method="post" enctype="multipart/form-data" autocomplete="off">
                {{csrf_field()}}
                <input type="hidden" name="folder_id" value="{{$folder->id}}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label>Nome do vídeo</label>
                                <input type="text" name="name" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="input-file">Vídeo <small>(Selecionar somente se desejar
                                        trocá-lo)</small></label>
                                <div class="input-group">
                                    <div class="custom-file">
                                        <input type="file" name="file" class="custom-file-input" id="input-video-file">
                                        <label class="custom-file-label" for="input-file">Escolha um vídeo </label>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Atualizar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<div class="modal fade" id="modalAddFolder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Adicionar Pasta</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form action="{{route('admin.folder.store')}}" method="post">
                {{csrf_field()}}
                <input type="hidden" name="folder_id" value="{{$folder->id}}">
                <div class="modal-body">
                    <div class="row">
                        <div class="col-12">
                            <div class="form-group">
                                <label for="input-name-folder">Nome</label>
                                <input type="text" class="form-control" name="name" id="input-name-folder" required>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer justify-content-between">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Fechar</button>
                    <button type="submit" class="btn btn-primary">Adicionar</button>
                </div>
            </form>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
@endsection