@extends('admin.templates.default')

@section('title', 'Dashboard')
@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark">Dashboard</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-lg-6 col-6">
                    <!-- small box -->
                    <div class="small-box bg-primary-ieaga">
                        <div class="inner">
                            <h3>0</h3>

                            <p>Produtos Cadastrados</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-tag"></i>
                        </div>
                        <a href="#" class="small-box-footer">Saiba mais <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-6 col-6">
                    <!-- small box -->
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3>10</h3>

                            <p>Orçamentos</p>
                        </div>
                        <div class="icon">
                            <i class="fas fa-list-alt"></i>
                        </div>
                        <a href="#" class="small-box-footer">Saiba mais <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <!-- <div class="col-lg-3 col-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3>44</h3>

                            <p>User Registrations</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3>65</h3>

                            <p>Unique Visitors</p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div> -->

            </div>
            <div class="row">
                <div class="col-sm-12">
                    <div class="card-body p-3">

                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Marca</th>
                                    <th>Modelo</th>
                                    <th>Status</th>
                                    <th>Total</th>
                                    <th>Data</th>
                                    <th>Ação</th>
                                </tr>
                            </thead>
                            <tbody>
                                @forelse($recentBudgets as $budget)
                                <tr>
                                    <td>{{$budget->code}}</td>
                                    <td>{{$budget->assembler}}</td>
                                    <td>{{$budget->model}}</td>
                                    <td>{{$budget->status}}</td>
                                    <td>$ {{convertMoneyUsaToBrazil($budget->total)}}</td>
                                    <td>{{convertDateUsaToBrazil($budget->created_at)}}</td>
                                    <td class="text-center align-middle py-0">
                                        <div class="btn-group btn-group-sm">
                                            <a href="{{route('admin.budget.edit', ['budgetId' => $budget->id])}}"
                                                class="btn btn-primary-ieaga">
                                                <i class="fa fa-edit"></i>
                                            </a>
                                            <button onclick="deleteItem(this,1)"
                                                data-href="{{route('admin.budget.delete', ['budgetId' => $budget->id])}}"
                                                class="btn btn-danger act-delete">
                                                <i class="fa fa-trash"></i>
                                            </button>
                                        </div>
                                    </td>
                                </tr>
                                @empty
                                <tr>
                                    <td colspan="5">Nenhum orçamento até o momento</td>
                                </tr>
                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection