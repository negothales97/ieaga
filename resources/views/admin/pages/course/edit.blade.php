@extends('admin.templates.default')

@section('title', 'Editar Curso')

@section('content')
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-1 text-dark">Editar Curso</h1>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <!-- Small boxes (Stat box) -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <form action="{{route('admin.course.update', ['course' => $course])}}" method="post">
                            {{csrf_field()}}
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-sm-6">
                                        <div class="form-group">
                                            <label for="input-name">Nome</label>
                                            <input type="text" value="{{old('name', $course->name)}}"
                                                class="form-control {{$errors->has('name') ? 'is-invalid' : ''}}"
                                                name="name" id="input-name">
                                            @if ($errors->has('name'))
                                            <span class="help-block">
                                                <small>
                                                    <strong>{{ $errors->first('name') }}</strong>
                                                </small>
                                            </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-footer">
                                <button class="btn btn-primary-ieaga float-right">Atualizar</button>
                            </div>
                        </form>

                    </div>
                    <!-- /.card -->
                </div>

                <!-- ./col -->
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Permissões</h4>
                            </div>
                            <div class="card-body">
                                <div class="row">
                                    <?php
                                    $countFolders = count($folders);
                                    $firstColQty = (int)($countFolders / 2);
                                    $i = 0;

                                    ?>
                                    <div class="col-md-6">
                                        @forelse($folders as $folder)
                                        <div class="form-group form-group-folder">
                                            <div class="checkbox">
                                                <label class="main-folder">
                                                    <input type="checkbox" name="folder" @if(in_array($folder->id,
                                                    $foldersId)) checked @endif class="checkbox-folder"
                                                    value="{{$folder->id}}">
                                                    {{$folder->name}}@if(count($folder->folders) > 0)
                                                    <small>*</small>
                                                    @endif
                                                </label>
                                            </div>
                                        </div>
                                        @component('admin.components.folders',['folder' => $folder,
                                        'foldersId' => $foldersId])
                                        @endcomponent

                                        <?php $i++; ?>

                                        @if($i == $firstColQty)
                                    </div>
                                    <div class="col-md-6">
                                        @endif

                                        @empty
                                        <p>Nenhuma categoria cadastrada</p>
                                        @endforelse
                                    </div>
                                </div>
                            </div>
                    </div>
                    <!-- /.card -->
                </div>

                <!-- ./col -->
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
</div>
@endsection
@section('scripts')
<script type="text/javascript">
$(document).ready( function(){
    inspectChecked();
});

$('.checkbox-folder').change(function() {
    var folderId = $(this).val();
    var courseId = "{{$course->id}}";
    $.get(`{{url('admin/course/folder/change')}}/${courseId}/${folderId}`, function() {});

    if (!$(this).is(':checked')) {
        changeIsChecked(folderId);
    }else{
        changeIsNotChecked(folderId);
    }
});

const inspectChecked = () => {
    $('.main-folder input').each( function(){
        let id = $(this).val();
        let isChecked = $(this).is(':checked');
        loadCheckeds( id, isChecked);
    });
}

const loadCheckeds = (folderId, isChecked) => {
    let folderDiv = $('#box-folder-' + folderId + ' input');
    if(folderDiv.length == 0){
        return;
    }
    if(!isChecked){
        $('#box-folder-' + folderId).addClass('no-checked');
    } else {
        $('#box-folder-' + folderId).removeClass('no-checked');
    }
    folderDiv.each( function(){
        loadCheckeds( $(this).val(), $(this).is(':checked') );
    });
}

const changeIsChecked = (folderId) => {
    let folderDiv = $('#box-folder-' + folderId + ' input');
    if(folderDiv.length == 0){
        return;
    }
    $('#box-folder-' + folderId).addClass('no-checked');

    folderDiv.prop("checked", false);
    if (!folderDiv.is(':checked')) {
        folderDiv.each( function(){
            changeIsChecked( $(this).val() );
        });
    }

}

const changeIsNotChecked = (folderId) => {
    let folderDiv = $('#box-folder-' + folderId + ' input');
    if(folderDiv.length == 0){
        return;
    }
    $('#box-folder-' + folderId).removeClass('no-checked');
    folderDiv.prop("checked", true);
    if (folderDiv.is(':checked')) {
        folderDiv.each( function(){
            changeIsNotChecked( $(this).val() );
        });
    }

}
</script>
@endsection