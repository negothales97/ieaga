<?php

use Illuminate\Support\Facades\File;


function convertMoneyBrazilToUsa($value)
{
    $value = str_replace(',', '.', str_replace('.', '', $value));
    $value = floatval($value);

    return $value;
}
function convertMoneyUsaToBrazil($value)
{
    $value = number_format($value, 2, ',', '.');

    return $value;
}

function convertDateBrazilToUsa($date)
{
    $date = implode("-", array_reverse(explode("/", $date)));

    return $date;
}

function convertDateUsaToBrazil($date)
{
    $date = date("d/m/Y", strtotime($date));
    return $date;
}

function getNameFile($originalImage, $name)
{
    $extension = '.' . File::extension($originalImage->getClientOriginalName());
    $fileName = $name . date('Ymd') . time() . microtime();
    $fileName = str_replace('.', '', $fileName);
    $fileName = str_replace(' ', '', $fileName) . $extension;

    return $fileName;
}

function getFileExtension($fileName)
{
    $arr = explode('.', $fileName);
    $extension = end($arr);
    return $extension;
}

function fileIsPdf($fileName)
{
    $extensions = ['pdf'];
    $fileExtension = getFileExtension($fileName);
    return in_array($fileExtension, $extensions);
}

function fileIsImage($fileName)
{
    $extensions = ['jpg', 'jpeg', 'png', 'gif'];
    $fileExtension = getFileExtension($fileName);
    return in_array($fileExtension, $extensions);
}

function getDocuments()
{
    $documents = \App\Models\Document::get();
    foreach($documents as $document){
        $filename = $document->file;
        $file = public_path().'/uploads/'.$document->file;
        $url = explode('.',$document->file);
        $path = public_path().'/uploads/';
        $pages = null;
        if($url[count($url) -1] == 'pdf'){
            $pages = \App\Services\DocumentService::generatePDFToImage($file,$filename, $path);
            $document->update(['pages' => $pages]);
        }
    }
}