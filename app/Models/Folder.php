<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Folder extends Model
{
    protected $fillable = [
        'name',
        'folder_id'
    ];

    public function folders()
    {
        return $this->hasMany('App\Models\Folder', 'folder_id');
    }
    public function videos()
    {
        return $this->hasMany('App\Models\Video', 'folder_id');
    }
    public function documents()
    {
        return $this->hasMany('App\Models\Document', 'folder_id');
    }

    public function folder()
    {
        return $this->belongsTo('App\Models\Folder', 'folder_id');
    }
}
