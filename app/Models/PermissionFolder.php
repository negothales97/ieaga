<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class PermissionFolder extends Model
{
    protected $fillable = [
        'course_id',
        'folder_id'
    ];
}
