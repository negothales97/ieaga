<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Document extends Model
{
    protected $fillable = [
        'name',
        'download',
        'file',
        'pages',
        'folder_id'
    ];
}
