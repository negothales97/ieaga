<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CustomerCourse extends Model
{
    protected $fillable = [
        'course_id',
        'customer_id',
        'date'
    ];
}
