<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable = [
        'name'
    ];

    public function customers()
    {
        return $this
        ->belongsToMany('App\Customer', 'customer_courses', 'course_id', 'customer_id')
        ->withPivot('date', 'id');
    }
    public function folders()
    {
        return $this
        ->belongsToMany('App\Models\Folder', 'permission_folders', 'course_id', 'folder_id');
    }
}
