<?php

namespace App\Services;

use App\Models\Course;

class CourseService
{
    public static function create(array $data): Course
    {
        return Course::create($data);
    }
    public static function update(array $data, Course $course): void
    {
        $course->fill($data);
        $course->save();
    }

    public static function delete(Course $course): void
    {
        $course->delete();
    }
}