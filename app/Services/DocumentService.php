<?php
namespace App\Services;

use App\Models\Document;

class DocumentService
{
    public static function create($data)
    {
        
        if(isset($data['file'])){
            $file = $data['file'];
            
            $filename = getNameFile($data['file'], $data['name']);
            $path = public_path().'/uploads/';
            $extension = $file->getClientOriginalExtension();
            $pages = null;
            if($extension == 'pdf'){
                $pages = DocumentService::generatePDFToImage($file,$filename, $path);
            }
            
            Document::create([
                'name' => $data['name'],
                'file' => $filename,
                'folder_id' => $data['folder_id'],
                'pages' => $pages
            ]);
            return $file->move($path, $filename);
        }
    }
    public static function update($data, Document $document)
    {
        $document->update($data);
    }

    public static function delete(Document $document)
    {
        $document->delete();
    }
    
    public static function generatePDFToImage($file, $filename, $path)
    {
        $arrayFile = explode('.', $filename);
        $arrayFile[count($arrayFile) -1]= '.jpg';
        $filename = implode("",$arrayFile);
        $imagick = new \Imagick();
        $imagick->setResolution(150, 150);
        $imagick->readImage($file);
        $imagick->writeImages($path.$filename, false);
        return $imagick->count();
    }
}