<?php

namespace App\Services;

use App\Customer;

class CustomerService
{
    public static function create(array $data): Customer
    {
        $data['password'] = bcrypt($data['password']);
        return Customer::create($data);
    }
    public static function update(array $data, Customer $customer): void
    {
        $customer->fill($data);
        $customer->save();
    }

    public static function delete(Customer $customer): void
    {
        $customer->delete();
    }
}
