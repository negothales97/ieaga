<?php

namespace App\Services;

use App\Customer;
use App\Models\Course;
use App\Models\Folder;
use App\Models\PermissionFolder;
use App\Services\PermissionFolderService;

class PermissionFolderService
{
    public static function create(Folder $folder, Course $course)
    {
        PermissionFolder::create([
            'folder_id' => $folder->id,
            'course_id' => $course->id,
        ]);
        if (count($folder->folders) > 0) {
            foreach ($folder->folders as $subfolder) {
                PermissionFolderService::create($subfolder, $course);
            }
        }
    }
    public static function delete(Folder $folder, Course $course, $permissionsFolder)
    {
        $folder = Folder::find($permissionsFolder->first()->folder_id);
        $permissionsFolder->delete();
        if (count($folder->folders) > 0) {
            foreach ($folder->folders as $subfolder) {
                $permissionsSubFolder = PermissionFolder::where('folder_id', $subfolder->id)
                    ->where('course_id', $course->id);
                PermissionFolderService::delete($subfolder, $course, $permissionsSubFolder);
            }
        }
    }
}
