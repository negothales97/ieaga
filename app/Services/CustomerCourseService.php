<?php

namespace App\Services;

use App\Customer;
use App\Models\CustomerCourse;

class CustomerCourseService
{
    public static function create(array $data, Customer $customer)
    {
        CustomerCourse::create([
            'course_id' => $data['course_id'],
            'customer_id' => $customer->id,
            'date' =>\convertDateBrazilToUsa($data['date'])
        ]);
    }
    public static function update(array $data, CustomerCourse $course)
    {
        $data['date'] = convertDateBrazilToUsa($data['date']);
        $course->fill($data);
        $course->update();
    }

    public static function delete(CustomerCourse $course)
    {
        $course->delete();
    }
}
