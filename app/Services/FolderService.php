<?php

namespace App\Services;

use App\Models\Folder;

class FolderService
{
    public static function create(array $data): Folder
    {
        return Folder::create($data);
    }
    public static function update(array $data, Folder $folder): void
    {
        $folder->update($data);
    }

    public static function delete(Folder $folder)
    {
        $folder->documents()->delete();
        $folder->videos()->delete();
        if($folder->folders){
            foreach($folder->folders as $subfolder){
                FolderService::delete($subfolder);
                $subfolder->delete();
            }
        }
        $folder->delete();
    }
}