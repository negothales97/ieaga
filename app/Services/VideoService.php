<?php
namespace App\Services;

use App\Models\Video;

class VideoService
{
    public static function create($data)
    {
        if (isset($data['file'])) {

            $file = $data['file'];
            $filename = getNameFile($data['file'], $data['name']);
            $path = public_path() . '/uploads/';
            Video::create([
                'name' => $data['name'],
                'file' => $filename,
                'folder_id' => $data['folder_id'],
            ]);
            return $file->move($path, $filename);
        }

    }

    public static function update(array $data, Video $video)
    {
        if (isset($data['file'])) {
            $file = $data['file'];
            $filename = getNameFile($data['file'], $data['name']);
            $path = public_path() . '/uploads/';
            $file->move($path, $filename);
        }else{
            $filename = $video->file;
        }
        $video->update([
            'name' => $data['name'],
            'file' => $filename,
        ]);
    }

    public static function delete(Video $video)
    {
        $video->delete();
    }
}
