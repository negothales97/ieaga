<?php

namespace App\Services;

class AccountService
{
    public static function update(array $data, $user)
    {
        if ($data['password'] == null) {
            $user->update([
                'name' => $data['name'],
                'email' => $data['email'],
            ]);
        } else {
            $user->update([
                'name' => $data['name'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
            ]);
        }
    }
}
