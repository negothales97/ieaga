<?php

namespace App\Services;

use App\Admin;

class AdminService
{
    public static function create(array $data): Admin
    {
        $data['password'] = bcrypt($data['password']);
        return Admin::create($data);
    }
    public static function update(array $data, Admin $admin): void
    {
        $admin->fill($data);
        $admin->save();
    }

    public static function delete(Admin $admin): void
    {
        $admin->delete();
    }
}