<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DocumentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if (!request()->is('*update*')) {
            $rules['name'] = 'required';
            $rules['file'] = 'required';
        } else {
            $rules['name'] = 'nullable';
            $rules['file'] = 'nullable';
        }
        return $rules;
    }
}
