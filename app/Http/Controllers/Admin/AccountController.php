<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Services\AccountService;
use App\Http\Controllers\Controller;
use App\Http\Requests\AccountRequest;

class AccountController extends Controller
{
    public function edit()
    {
        $admin = auth()->guard('admin')->user();
        return view('admin.pages.account.edit')
        ->with('admin', $admin);
    }
    
    public function update(AccountRequest $request)
    {
        $admin = auth()->guard('admin')->user();
        try {
            AccountService::update($request->all(), $admin);
        } catch (\Exception $e) {
            return redirect()->back()
            ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->back()
        ->with('success', 'Dados atualizados com sucesso');
    }
}
