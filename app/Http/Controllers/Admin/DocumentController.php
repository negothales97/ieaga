<?php

namespace App\Http\Controllers\Admin;

use App\Models\Document;
use Illuminate\Http\Request;
use App\Services\DocumentService;
use App\Http\Controllers\Controller;
use App\Http\Requests\DocumentRequest;

class DocumentController extends Controller
{
    public function store(DocumentRequest $request)
    {
        DocumentService::create($request->all());
        return redirect()->back()
            ->with('success', 'Documento adicionado com sucesso');
    }
    public function update(DocumentRequest $request, Document $document)
    {
        DocumentService::update($request->all(), $document);
        return redirect()->back()
            ->with('success', 'Documento alterado com sucesso');
    }

    public function change(Document $document)
    {
        $document->update([
            'download' =>$document->download == 0 ? 1 : 0
        ]);
        return redirect()->back()->with('success', 'Documento alterado com sucesso');
    }

    public function delete(Document $document)
    {
        DocumentService::delete($document);
        return redirect()->back()->with('success', 'Documento removido com sucesso');

    }
}
