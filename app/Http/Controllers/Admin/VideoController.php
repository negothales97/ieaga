<?php

namespace App\Http\Controllers\Admin;

use App\Models\Video;
use App\Services\VideoService;
use App\Http\Requests\VideoRequest;
use App\Http\Controllers\Controller;

class VideoController extends Controller
{
    public function store(VideoRequest $request)
    {
        try {
            VideoService::create($request->all());
        } catch (\Exception $e) {
            dd($e->getMessage());
        }

        return redirect()->back()
            ->with('success', 'Video adicionado com sucesso');
    }
    public function update(VideoRequest $request, Video $video)
    {
        try {
            VideoService::update($request->all(), $video);
        } catch (\Exception $e) {
            dd($e->getMessage());
        }
        return redirect()->back()
            ->with('success', 'Video atualizado com sucesso');
    }

    public function delete(Video $video)
    {
        VideoService::delete($video);
        return \redirect()->back()
            ->with('success', 'Video removido com sucesso');
    }
}
