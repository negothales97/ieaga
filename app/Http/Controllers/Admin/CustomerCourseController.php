<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Models\Course;
use App\Models\CustomerCourse;
use App\Http\Controllers\Controller;
use App\Services\CustomerCourseService;
use App\Http\Requests\CustomerCourseRequest;

class CustomerCourseController extends Controller
{
    public function store(Customer $customer, CustomerCourseRequest $request)
    {
        try {
            CustomerCourseService::create($request->all(), $customer);
        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->back()
            ->with('success', 'Limite adicionado com sucesso');
    }
    public function update(CustomerCourse $course, CustomerCourseRequest $request)
    {
        try {
            CustomerCourseService::update($request->all(), $course);
        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->back()
            ->with('success', 'Limite atualizado com sucesso');
    }

    public function delete(CustomerCourse $course)
    {
        try {
            CustomerCourseService::delete($course);
        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->back()
            ->with('status', 'Limite removido com sucesso');
    }
}
