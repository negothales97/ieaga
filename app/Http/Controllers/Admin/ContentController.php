<?php

namespace App\Http\Controllers\Admin;

use App\Models\Folder;
use App\Http\Controllers\Controller;

class ContentController extends Controller
{
    public function index()
    {
        $folders = Folder::whereNull('folder_id')->get();
        return view('admin.pages.content.index')
            ->with('folders', $folders);
    }

    public function show(Folder $folder)
    {
        $folderMaster = Folder::find($folder->folder_id);
        $isMaster = $folderMaster && $folderMaster->folder_id == null;
        return view('admin.pages.content.show')
        ->with('isMaster', $isMaster)
        ->with('folder', $folder);
    }
}
