<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\CourseRequest;
use App\Models\Course;
use App\Models\Folder;
use App\Models\PermissionFolder;
use App\Services\CourseService;
use App\Services\PermissionFolderService;

class CourseController extends Controller
{
    public function index()
    {
        $courses = Course::paginate(10);
        return view('admin.pages.course.index')
            ->with('resources', $courses);
    }

    public function create()
    {
        return view('admin.pages.course.create');
    }

    public function store(CourseRequest $request)
    {
        try {
            $course = CourseService::create($request->all());
        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->route('admin.course.edit', ['course' => $course])
            ->with('success', 'Curso adicionado com sucesso');
    }

    public function edit(Course $course)
    {
        $folders = Folder::whereNull('folder_id')->get();
        $foldersId = PermissionFolder::where('course_id', $course->id)->pluck('folder_id')->toArray();
        return view('admin.pages.course.edit')
            ->with('folders', $folders)
            ->with('foldersId', $foldersId)
            ->with('course', $course);
    }
    public function update(CourseRequest $request, Course $course)
    {
        try {
            CourseService::update($request->all(), $course);
        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->back()
            ->with('success', 'Curso atualizado com sucesso');
    }

    public function delete(Course $course)
    {
        $course->customers()->sync([]);
        $course->folders()->sync([]);
        $course->delete();
        return redirect()->back()
            ->with('success', 'Curso removido por completo');
    }
    public function change(Course $course, Folder $folder)
    {
        $permissionsFolder = PermissionFolder::where('folder_id', $folder->id)
            ->where('course_id', $course->id);

        if ($permissionsFolder->count() == 0) {
            PermissionFolderService::create($folder, $course);
        } else {
            PermissionFolderService::delete($folder, $course, $permissionsFolder);
        }
    }
}
