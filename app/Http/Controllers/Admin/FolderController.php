<?php

namespace App\Http\Controllers\Admin;

use App\Models\Folder;
use App\Services\FolderService;
use App\Http\Controllers\Controller;
use App\Http\Requests\FolderRequest;

class FolderController extends Controller
{
    public function store(FolderRequest $request)
    {
        FolderService::create($request->all());
        return \redirect()->back()
            ->with('success', 'Pasta adicionada com sucesso');
    }

    public function update(FolderRequest $request, Folder $folder)
    {
        FolderService::update($request->all(), $folder);
        return \redirect()->back()
            ->with('success', 'Pasta atualizada com sucesso');
    }

    public function delete(Folder $folder)
    {
        FolderService::delete($folder);
        return \redirect()->back()
            ->with('success', 'Pasta removida com sucesso');
    }
}
