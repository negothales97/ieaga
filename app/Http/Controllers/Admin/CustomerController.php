<?php

namespace App\Http\Controllers\Admin;

use App\Customer;
use App\Models\Course;
use App\Models\Folder;
use Illuminate\Http\Request;
use App\Models\PermissionFolder;
use App\Services\CustomerService;
use App\Http\Controllers\Controller;
use App\Http\Requests\CustomerRequest;
use App\Services\PermissionFolderService;

class CustomerController extends Controller
{
    public function index(Request $request)
    {
        $customers = new Customer();
        if ($request->has('name')) {
            if (request('name') != '') {
                $customers = $customers->where('name', 'like', '%' . request('name') . '%');
            }
        }
        if ($request->has('email')) {
            if (request('email') != '') {
                $customers = $customers->where('email', 'like', '%' . request('email') . '%');
            }
        }
        // if ($request->has('course_id')) {
        //     if (request('course_id') != '') {
        //         $customers = $customers->where('course_id', request('course_id'));
        //     }
        // }
        $customers = $customers->paginate(10);
        $courses = Course::get();
        return view('admin.pages.customer.index')
            ->with('resources', $customers)
            ->with('courses', $courses);
    }

    public function create()
    {
        return view('admin.pages.customer.create');
    }

    public function store(CustomerRequest $request)
    {
        try {
            $customer = CustomerService::create($request->all());
        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->route('admin.customer.edit', ['customer' => $customer])
            ->with('success', 'Usuário adicionado com sucesso');
    }

    public function edit(Customer $customer)
    {
        $courses = Course::get();
        return view('admin.pages.customer.edit')
            ->with('courses', $courses)
            ->with('customer', $customer);
    }

    public function update(CustomerRequest $request, Customer $customer)
    {
        try {
            $customer = CustomerService::update($request->all(), $customer);
        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->back()
            ->with('success', 'Usuário atualizado com sucesso');
    }

    public function delete(Customer $customer)
    {
        try {
            CustomerService::delete($customer);
        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->back()
            ->with('status', 'Usuário removido com sucesso');
    }

    

}
