<?php

namespace App\Http\Controllers\Admin;

use App\Admin;
use App\Models\Course;
use Illuminate\Http\Request;
use App\Services\AdminService;
use App\Http\Requests\AdminRequest;
use App\Http\Controllers\Controller;

class AdminController extends Controller
{

    public function index(Request $request)
    {
        $admins = Admin::paginate(10);
        return view('admin.pages.admin.index')
            ->with('resources', $admins);
    }

    public function create()
    {
        return view('admin.pages.admin.create');
    }

    public function store(AdminRequest $request)
    {
        try {
            $admin = AdminService::create($request->all());
        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->route('admin.admin.edit', ['admin' => $admin])
            ->with('success', 'Administrador adicionado com sucesso');
    }

    public function edit(Admin $admin)
    {
        $courses = Course::get();
        return view('admin.pages.admin.edit')
            ->with('courses', $courses)
            ->with('admin', $admin);
    }

    public function update(AdminRequest $request, Admin $admin)
    {
        try {
            $admin = AdminService::update($request->all(), $admin);
        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->back()
            ->with('success', 'Administrador atualizado com sucesso');
    }

    public function delete(Admin $admin)
    {
        try {
            AdminService::delete($admin);
        } catch (\Exception $e) {
            return redirect()->back()
                ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->back()
            ->with('status', 'Administrador removido com sucesso');
    }
}
