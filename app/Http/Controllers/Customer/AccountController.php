<?php

namespace App\Http\Controllers\Customer;

use Illuminate\Http\Request;
use App\Services\AccountService;
use App\Http\Controllers\Controller;
use App\Http\Requests\AccountRequest;

class AccountController extends Controller
{
    public function edit()
    {
        $customer = auth()->guard('customer')->user();
        return view('customer.pages.account.edit')
        ->with('customer', $customer);
    }
    
    public function update(AccountRequest $request)
    {
        $customer = auth()->guard('customer')->user();
        try {
            AccountService::update($request->all(), $customer);
        } catch (\Exception $e) {
            return redirect()->back()
            ->with('error', 'Tivemos um problema com o servidor, entre em contato com o administrador.');
        }
        return redirect()->back()
        ->with('success', 'Dados atualizados com sucesso');
    }
}
