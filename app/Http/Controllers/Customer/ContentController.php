<?php

namespace App\Http\Controllers\Customer;

use App\Http\Controllers\Controller;
use App\Models\Folder;

class ContentController extends Controller
{
    public function index()
    {
        $customer = auth()->guard('customer')->user();
        $courseId = $customer->courses()->whereDate('date', '>=', date("Y-m-d"))->pluck('courses.id')->toArray();
        $folders = Folder::join('permission_folders', 'permission_folders.folder_id', '=', 'folders.id')
            ->whereIn('course_id', $courseId)
            ->whereNull('folders.folder_id')
            ->select('folders.*')
            ->orderby('name', 'asc')
            ->get();
        return view('customer.pages.content.index')
            ->with('folders', $folders);
    }

    public function show(Folder $folder)
    {
        $customer = auth()->guard('customer')->user();
        $courseId = $customer->courses()->whereDate('date', '>=', date("Y-m-d"))->pluck('courses.id')->toArray();
        $subfolders = $folder
            ->join('permission_folders', 'permission_folders.folder_id', '=', 'folders.id')
            ->whereIn('course_id', $courseId)
            ->where('folders.folder_id', $folder->id)
            ->select('folders.*')
            ->orderby('name', 'asc')
            ->get();
        return view('customer.pages.content.show')
            ->with('folder', $folder)
            ->with('subfolders', $subfolders);
    }
}
