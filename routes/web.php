<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect( route('login') );
});

Route::group(['prefix' => 'admin'], function () {
    Route::get('/login', 'AdminAuth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'AdminAuth\LoginController@login');
    Route::get('/logout', 'AdminAuth\LoginController@logout')->name('logout');

    Route::get('/register', 'AdminAuth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'AdminAuth\RegisterController@register');

    Route::post('/password/email', 'AdminAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'AdminAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'AdminAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'AdminAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin', 'middleware' => 'admin'], function () {
    Route::get('account', 'AccountController@edit')->name('account.edit');
    Route::post('account', 'AccountController@update')->name('account.update');

    Route::group(['prefix' => 'admin', 'as' => 'admin.'], function () {
        Route::get('/', 'AdminController@index')->name('index');
        Route::get('/create', 'AdminController@create')->name('create');
        Route::post('/store', 'AdminController@store')->name('store');
        Route::get('/edit/{admin}', 'AdminController@edit')->name('edit');
        Route::get('/folder/change/{admin}/{folder}', 'AdminController@change')->name('change');
        Route::post('/update/{admin}', 'AdminController@update')->name('update');
        Route::get('/delete/{admin}', 'AdminController@delete')->name('delete');
    });
    Route::group(['prefix' => 'customer', 'as' => 'customer.'], function () {
        Route::get('/', 'CustomerController@index')->name('index');
        Route::get('/create', 'CustomerController@create')->name('create');
        Route::post('/store', 'CustomerController@store')->name('store');
        Route::get('/edit/{customer}', 'CustomerController@edit')->name('edit');
        Route::get('/folder/change/{customer}/{folder}', 'CustomerController@change')->name('change');
        Route::post('/update/{customer}', 'CustomerController@update')->name('update');
        Route::get('/delete/{customer}', 'CustomerController@delete')->name('delete');
        Route::group(['prefix' => 'course', 'as' => 'course.'], function () {
            Route::post('/store/{customer}', 'CustomerCourseController@store')->name('store');
            Route::post('/update/{course}', 'CustomerCourseController@update')->name('update');
            Route::get('/delete/{course}', 'CustomerCourseController@delete')->name('delete');
        });
    });
    Route::group(['prefix' => 'course', 'as' => 'course.'], function () {
        Route::get('/', 'CourseController@index')->name('index');
        Route::get('/create', 'CourseController@create')->name('create');
        Route::post('/store', 'CourseController@store')->name('store');
        Route::get('/edit/{course}', 'CourseController@edit')->name('edit');
        Route::get('/folder/change/{course}/{folder}', 'CourseController@change')->name('change');
        Route::post('/update/{course}', 'CourseController@update')->name('update');
        Route::get('/delete/{course}', 'CourseController@delete')->name('delete');
    });
    Route::group(['prefix' => 'conteudo', 'as' => 'content.'], function () {
        Route::get('/', 'ContentController@index')->name('index');
        Route::get('/show/{folder}', 'ContentController@show')->name('show');
    });
    Route::group(['prefix' => 'folder', 'as' => 'folder.'], function () {
        Route::post('/store', 'FolderController@store')->name('store');
        Route::post('/update/{folder}', 'FolderController@update')->name('update');
        Route::get('/delete/{folder}', 'FolderController@delete')->name('delete');
    });
    Route::group(['prefix' => 'video', 'as' => 'video.'], function () {
        Route::post('/store', 'VideoController@store')->name('store');
        Route::post('/update/{video}', 'VideoController@update')->name('update');
        Route::get('/delete/{video}', 'VideoController@delete')->name('delete');
    });
    Route::group(['prefix' => 'document', 'as' => 'document.'], function () {
        Route::post('/store', 'DocumentController@store')->name('store');
        Route::post('/update/{document}', 'DocumentController@update')->name('update');
        Route::get('/change/{document}', 'DocumentController@change')->name('change');
        Route::get('/delete/{document}', 'DocumentController@delete')->name('delete');
    });
});

Route::group(['prefix' => 'customer'], function () {
    Route::get('/login', 'CustomerAuth\LoginController@showLoginForm')->name('login');
    Route::post('/login', 'CustomerAuth\LoginController@login');
    Route::get('/logout', 'CustomerAuth\LoginController@logout')->name('logout');

    Route::get('/register', 'CustomerAuth\RegisterController@showRegistrationForm')->name('register');
    Route::post('/register', 'CustomerAuth\RegisterController@register');

    Route::post('/password/email', 'CustomerAuth\ForgotPasswordController@sendResetLinkEmail')->name('password.request');
    Route::post('/password/reset', 'CustomerAuth\ResetPasswordController@reset')->name('password.email');
    Route::get('/password/reset', 'CustomerAuth\ForgotPasswordController@showLinkRequestForm')->name('password.reset');
    Route::get('/password/reset/{token}', 'CustomerAuth\ResetPasswordController@showResetForm');
});

Route::group(['prefix' => 'customer', 'as' => 'customer.', 'middleware' => 'customer', 'namespace' => 'Customer'], function () {
    Route::get('account', 'AccountController@edit')->name('account.edit');
    Route::post('account', 'AccountController@update')->name('account.update');
    Route::group(['prefix' => 'conteudo', 'as' => 'content.'], function () {
        Route::get('/', 'ContentController@index')->name('index');
        Route::get('/show/{folder}', 'ContentController@show')->name('show');
    });
});
Route::get('/rodar-funcao', function(){
    getDocuments();
});