<?php

use Illuminate\Database\Seeder;

class AdminsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert([
            [
                'name' => 'Thales Serra',
                'email' => 'thales@imaxinformatica.com.br',
                'password' => bcrypt('Thales100%')
            ],
            [
                'name' => 'Lucas Borelli',
                'email' => 'lucas@imaxinformatica.com.br',
                'password' => bcrypt('.Welcome09')
            ],
            [
                'name' => 'Igor Nascimento',
                'email' => 'igor@imaxinformatica.com.br',
                'password' => bcrypt('.Welcome09')
            ],
            [
                'name' => 'Milton',
                'email' => 'milton@ieaga.org',
                'password' => bcrypt('01020304')
            ],
        ]);
    }
}
